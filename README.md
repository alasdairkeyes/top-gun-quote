# top-gun-quote

Get a random quote from Top Gun


## Usage

* Get a quote

```
./tgquote
```

* Get just the quote

Quotes are usually preceeded by "tgquote:  ", add `-q` to ensure you get nothing but hardcore Top Gun.

```
./tgquote -q

```

* Need some help

```
./tgquote -h
```


## Top Gun Day

On May 13th, tgquote will remind you that it's goddam Top Gun Day https://www.topgunday.com/ and you should pay your respects by quoting Top Gun to the pleasure and thrill of your friends and colleagues.


## Top Tips

* Why not add this to your `.bashrc` or `.bash_profile` scripts to get a kick-ass Top Gun quote each time you get a console.
* Add tgquote to your CI/CD build and brighten up your co-workers software builds.
* Your website could use some more Top Gun, why not call `tgquote` from your site to let your customers know how balls-to-the-wall Top Gun you really are.


## Missing quotes

If you have a quote that you want included, let me know. All quotes are added at my discretion, if you don't like it, fork this repo and create your own.


## License

Top Gun is too good to withhold from the world, this is released under the GNU v3 license, see LICENSE file for full information


## Site

https://www.gitlab.com/alasdairkeyes/top-gun-quote
